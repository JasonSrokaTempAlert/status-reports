source(file.path(GitHomeDir,'Utilities/Data Access','DevSnapshotAccess.R'))

print(paste('GenDeviceByDateReport::  reading GroupRollupsDT_wStoreInfo from file:',Filename_StoreStatusSummaries))
GroupRollupsDT_wStoreInfo<-read.csv(file=Filename_StoreStatusSummaries,stringsAsFactors = FALSE)

if(length(which(names(GroupRollupsDT_wStoreInfo)=='Area'))==0){
  print('CVS_AugStoreStatus script: no existing Area column in store file, augmenting the file with Area/Region/District'  )
  ARDQuery<-'select storeid StoreID, storegroupid GroupID, area Area, region Region, district District from [reporting.data].dbo.cvsdata with(nolock)'
ARDDF<-SubmitPrecipioQuery(ARDQuery)

AugGroupRollupsDT_wStoreInfo<-merge(GroupRollupsDT_wStoreInfo,ARDDF,by='GroupID',all.x=TRUE)
write.csv(AugGroupRollupsDT_wStoreInfo,file=Filename_StoreStatusSummaries,row.names = FALSE)
} else {
  print('CVS_AugStoreStatus script: found existing Area column in store file, not editing the file'  )
}