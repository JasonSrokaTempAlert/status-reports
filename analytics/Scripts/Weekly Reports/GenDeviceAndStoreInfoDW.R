library(data.table)
GenDeviceAndStoreInfoDW<-function(DataWarehousePrefix,AccountID,DataDir){
  print(paste('GendeviceAndStoreInfoDW::  called with DataWarehousePrefix:',DataWarehousePrefix,'AccountID:',AccountID,'DataDir:',DataDir))
  DataAccessScriptsDir<-file.path(GitHomeDir,'Utilities/Data Access')
  
  ####  Load other tools
  source(file.path(DataAccessScriptsDir,'DevSnapshotAccess.R'))
  
  ####  libraries
  library(sqldf)
  
  ####  File Names
  Filename_StoreInfoDB<-file.path(CONFIG_DataDir,paste(DataWarehousePrefix,'StoreAndInstallInfo.csv',sep=''))
  # Filename_DeviceAndStoreInfo<-file.path(CONFIG_DataDir,paste(DataWarehousePrefix,'DeviceAndStoreDF_Base.csv',sep=''))
  FileName_TemperatureFirstMsgData<-file.path(CONFIG_DataDir,'TemperatureFirstMsgData.csv')
  FileName_PowerFirstMsgData<-file.path(CONFIG_DataDir,'PowerFirstMsgData.csv')
  FileName_ErrorLogFirstMsgData<-file.path(CONFIG_DataDir,'ErrorLogFirstMsgData.csv')
  FileName_DeviceInstallDB<-file.path(CONFIG_DataDir,'DeviceInstallationDatesDB.csv')
  
  
  StoreInstallDatesQuery<-paste("select g.DisplayName GroupName, min(a.ActionDate) StoreInstallDateTime, g.GroupID from [precipio.snapshot.accounts].dbo.device d with(nolock)
  inner join (select substring(action, 13, 20) as did, actiondate from [precipio.snapshot.accounts].dbo.auditlog with(nolock) where userid in (select userid from [precipio.snapshot.accounts].dbo.[user] where accountid =",AccountID,") and ItemType = 'apiV3 - DeviceUpsert') a
  on (convert(varchar(20), d.deviceid) = a.did)
  inner join [precipio.snapshot.accounts].dbo.[group] g with(nolock) on (d.groupid = g.groupid)
  and d.AccountID =",AccountID, "group by g.DisplayName, g.GroupID order by g.DisplayName")
  StoreInstallDatesDF<-SubmitPrecipioQuery(StoreInstallDatesQuery)   
  
  StoreInstallDatesDF$StoreInstallDate<-substr(as.character(StoreInstallDatesDF$StoreInstallDateTime),1,10)
  StoreInfoDF<-StoreInstallDatesDF[,c('GroupID','StoreInstallDate')]
  StoreGroupIDsToIgnore<-StoreInstallDatesDF$GroupID[which(grepl('RMA - Replace or Repair',StoreInstallDatesDF$GroupName,fixed = TRUE))]
  StoreGroupIDsToIgnore<-ifelse(length(StoreGroupIDsToIgnore)==0,0,StoreGroupIDsToIgnore)
  
  ######     Device Info DF
  DeviceInfoQuery<-paste('select DeviceID, NickName DeviceLabel, G.DisplayName GroupName, D.InstallDate DeviceInstallDate, D.DateDeleted, D.LastActivity, D.GroupID, D.AccountID, D.HardwareFamily, D.HardwareVersion, D.Firmware,
                         LastPower, LastPowerDate, Latitude DeviceLat, Longitude DeviceLong, D.Gateway from [precipio.snapshot.accounts].dbo.Device D with(nolock) join [precipio.snapshot.accounts].dbo.[Group] G with(nolock) on D.GroupID = G.GroupID
                         where D.AccountID =',AccountID,'and  D.DateDeleted is NULL and D.GroupID is not null and D.GroupID not in (',paste(StoreGroupIDsToIgnore,collapse=','),')')
  DeviceInfoDF<-SubmitPrecipioQuery(DeviceInfoQuery)
  # browser()
  DeviceInfoDF$StrDeviceID<-paste('Device:',DeviceInfoDF$DeviceID,sep='')
  DeviceInfoDF$GatewayStrDeviceID<-paste('Device:',DeviceInfoDF$Gateway,sep='')
  DeviceInfoDF$DeviceType<-'UNKNOWN'
  DeviceInfoDF$DeviceType<-ifelse(grepl('Device:[89|99]',DeviceInfoDF$StrDeviceID),'Gateway',DeviceInfoDF$DeviceType)
  DeviceInfoDF$DeviceType<-ifelse(grepl('Device:11',DeviceInfoDF$StrDeviceID),'ZPoint Node',DeviceInfoDF$DeviceType)
  DeviceInfoDF$DeviceType<-ifelse(!is.na(DeviceInfoDF$HardwareFamily) & DeviceInfoDF$HardwareFamily == 50 & DeviceInfoDF$HardwareVersion == 1,'ZPoint Node', DeviceInfoDF$DeviceType)
  DeviceInfoDF$DeviceType<-ifelse(!is.na(DeviceInfoDF$HardwareFamily) & DeviceInfoDF$HardwareFamily %in% c(2,3),'Gateway', DeviceInfoDF$DeviceType)
# browser()
  DeviceAndStoreDF_Base<-merge(StoreInfoDF,DeviceInfoDF,by='GroupID',all.y=TRUE)
  write.csv(DeviceAndStoreDF_Base,file=Filename_DeviceAndStoreDW,row.names=FALSE)
  
  ####  If the Install Date in the prior query has not populated all the sensors, then we will execute the original approach to determining an install date.
  ###  In the process, we create a table of DeviceIDs and Install Dates that can be delivered to Sally for updating the production DB.
  DevicesNeverInstalled<-DeviceInfoDF$StrDeviceID[which(is.na(DeviceInfoDF$DeviceInstallDate))]
  
  ####  Find the most recent reading - used for time-since-last report
  MostRecentTemperatureQuery<-'select top 1 ReadingDate from [precipio.snapshot.readings].dbo.Temperature T with(nolock) order by TemperatureID desc'
  MostRecentReadingDate<-SubmitPrecipioQuery(MostRecentTemperatureQuery)
  DaysSinceLastReport<-round(difftime(strptime(MostRecentReadingDate,format='%Y-%m-%d %H:%M:%S'),DeviceAndStoreDF_Base$LastActivity,units='days'),0)
  DeviceAndStoreDF_Base$DaysSinceLastReport<-ifelse(is.na(DaysSinceLastReport),NA,pmax(0,round(DaysSinceLastReport)))
  
  # browser()
  
  if(length(DevicesNeverInstalled)>0){
    
    
    ####   First Messages checks
    
    #######  Start of Power
    ##
    ####  Check for existing processed data
    FirstMsg_Power_ArchiveFilesList<-dir(CONFIG_DataDir,pattern = paste(DataWarehousePrefix,'_FirstMsgArchive_Power_.*csv',sep=''))
    if(length(FirstMsg_Power_ArchiveFilesList)==0){
      FirstMsg_PowerQuery_Archive<-paste('select P.DeviceID, Min(ReadingDate) FirstMsg_Power from [precipio.archive].dbo.[Power] P with(nolock)
  join [precipio.snapshot.accounts].dbo.Device D on P.DeviceID = D.DeviceID where D.AccountID =',CONFIG_AccountID,'and
  D.DateDeleted is NULL group by P.DeviceID')
      FirstMsg_PowerDF_Archive<-SubmitPrecipioQuery(FirstMsg_PowerQuery_Archive)
      FirstMsg_PowerDF_Archive$StrDeviceID<-paste('Device:',FirstMsg_PowerDF_Archive$DeviceID,sep='')
      FirstMsg_PowerDF_Archive$FirstMsgDate_Power<-substr(FirstMsg_PowerDF_Archive$FirstMsg_Power,1,10)
      MaxPowerIDQuery<-'select top 1 PowerID MaxPowerID from [precipio.archive].dbo.[Power] P with(nolock) order by PowerID desc'
      MaxPowerID<-unlist(SubmitPrecipioQuery(MaxPowerIDQuery))
      FileName_PowerFirstMsgData_Archive<-file.path(CONFIG_DataWarehouseDir,paste(DataWarehousePrefix,'_FirstMsgArchive_Power_',MaxPowerID,'.csv',sep=''))
      write.csv(FirstMsg_PowerDF_Archive[,!names(FirstMsg_PowerDF_Archive) == 'DeviceID'],file=FileName_PowerFirstMsgData_Archive,row.names=FALSE)
    }
    FirstMsg_Power_ArchiveFilesList<-dir(CONFIG_DataWarehouseDir,pattern = paste(DataWarehousePrefix,'_FirstMsgArchive_Power_.*csv',sep=''))
    ###  If there is more than one file, take the last in the last - very roughly should be the most recent
    FirstMsg_Power_ArchiveFile<-file.path(CONFIG_DataWarehouseDir,FirstMsg_Power_ArchiveFilesList[length(FirstMsg_Power_ArchiveFilesList)])
    
    FirstMsg_PowerDF_Archive<-read.csv(FirstMsg_Power_ArchiveFile,header=TRUE,stringsAsFactors = FALSE)
    ###  Extract MaxID
    ###  Remove the basic file name and the extension
    MaxPowerID_Archive<-gsub(paste(DataWarehousePrefix,'_FirstMsgArchive_Power_',sep=''),'',gsub('.csv','',basename(FirstMsg_Power_ArchiveFile)))
    
    ####  Now do the incremental query
    MaxPowerIDQuery<-'select top 1 PowerID MaxPowerID from [precipio.snapshot.readings].dbo.[Power] P with(nolock) order by PowerID desc'
    MaxPowerID<-unlist(SubmitPrecipioQuery(MaxPowerIDQuery))
    FileName_PowerFirstMsgData_Archive<-file.path(CONFIG_DataWarehouseDir,paste(DataWarehousePrefix,'_FirstMsgArchive_Power_',MaxPowerID,'.csv',sep=''))
    FirstMsg_PowerQuery<-paste('select P.DeviceID, Min(ReadingDate) FirstMsg_Power from [precipio.snapshot.readings].dbo.[Power] P with(nolock)
                           join [precipio.snapshot.accounts].dbo.Device D on P.DeviceID = D.DeviceID where D.AccountID =',CONFIG_AccountID,'and D.DateDeleted is NULL and P.PowerID >',MaxPowerID_Archive,'group by P.DeviceID')
    FirstMsg_PowerDF<-SubmitPrecipioQuery(FirstMsg_PowerQuery)
    # browser()
    if(nrow(FirstMsg_PowerDF)>0){
      
      FirstMsg_PowerDF$StrDeviceID<-paste('Device:',FirstMsg_PowerDF$DeviceID,sep='')
      FirstMsg_PowerDF$FirstMsgDate_Power<-substr(FirstMsg_PowerDF$FirstMsg_Power,1,10)
      ####  Merge the new with the old
      FirstMsg_PowerDF_Full<-merge(FirstMsg_PowerDF[,c('StrDeviceID','FirstMsgDate_Power')],
                                   FirstMsg_PowerDF_Archive[,c('StrDeviceID','FirstMsgDate_Power')],
                                   by='StrDeviceID',
                                   suffixes=c('NewReadings','ArchivedReadings'),all=TRUE)
      FirstMsg_PowerDF_Full$FirstMsgDate_Power<-pmin(FirstMsg_PowerDF_Full$FirstMsgDate_PowerNewReadings,
                                                     FirstMsg_PowerDF_Full$FirstMsgDate_PowerArchivedReadings,na.rm = TRUE)
      write.csv(FirstMsg_PowerDF_Full[,c('StrDeviceID','FirstMsgDate_Power')],file=FileName_PowerFirstMsgData_Archive,row.names=FALSE)
    }
    ######  End of Power
    
    
    #######  Start of Temperature
    ##
    ####  Check for existing processed data
    FirstMsg_Temperature_ArchiveFilesList<-dir(CONFIG_DataWarehouseDir,pattern = paste(DataWarehousePrefix,'_FirstMsgArchive_Temperature_.*csv',sep=''))
    # browser()
    if(length(FirstMsg_Temperature_ArchiveFilesList)==0){
      FirstMsg_TemperatureQuery_Archive<-paste('select D.DeviceID, Min(ReadingDate) FirstMsg_Temperature from [precipio.archive].dbo.[Temperature] T with(nolock)
                                       join [precipio.snapshot.accounts].dbo.Sensor S on T.SensorID = S.SensorID 
                                       join [precipio.snapshot.accounts].dbo.Device D on S.DeviceID = D.DeviceID where D.AccountID =',CONFIG_AccountID,'and
                                       D.DateDeleted is NULL group by D.DeviceID')
      FirstMsg_TemperatureDF_Archive<-SubmitPrecipioQuery(FirstMsg_TemperatureQuery_Archive)
      FirstMsg_TemperatureDF_Archive$StrDeviceID<-paste('Device:',FirstMsg_TemperatureDF_Archive$DeviceID,sep='')
      FirstMsg_TemperatureDF_Archive$FirstMsgDate_Temperature<-substr(FirstMsg_TemperatureDF_Archive$FirstMsg_Temperature,1,10)
      MaxTemperatureIDQuery<-'select top 1 TemperatureID MaxTemperatureID from [precipio.archive].dbo.[Temperature] with(nolock) order by TemperatureID desc'
      MaxTemperatureID<-unlist(SubmitPrecipioQuery(MaxTemperatureIDQuery))
      FileName_TemperatureFirstMsgData_Archive<-file.path(CONFIG_DataWarehouseDir,paste(DataWarehousePrefix,'_FirstMsgArchive_Temperature_',MaxTemperatureID,'.csv',sep=''))
      write.csv(FirstMsg_TemperatureDF_Archive[,!names(FirstMsg_TemperatureDF_Archive) == 'DeviceID'],file=FileName_TemperatureFirstMsgData_Archive,row.names=FALSE)
    }
    FirstMsg_Temperature_ArchiveFilesList<-dir(CONFIG_DataWarehouseDir,pattern = paste(DataWarehousePrefix,'_FirstMsgArchive_Temperature_.*csv',sep=''))
    ###  If there is more than one file, take the last in the last - very roughly should be the most recent
    FirstMsg_Temperature_ArchiveFile<-file.path(CONFIG_DataWarehouseDir,FirstMsg_Temperature_ArchiveFilesList[length(FirstMsg_Temperature_ArchiveFilesList)])
    
    FirstMsg_TemperatureDF_Archive<-read.csv(FirstMsg_Temperature_ArchiveFile,header=TRUE,stringsAsFactors = FALSE)
    ###  Extract MaxID
    ###  Remove the basic file name and the extension
    MaxTemperatureID_Archive<-gsub(paste(DataWarehousePrefix,'_FirstMsgArchive_Temperature_',sep=''),'',gsub('.csv','',basename(FirstMsg_Temperature_ArchiveFile)))
    
    ####  Now do the incremental query
    MaxTemperatureIDQuery<-'select top 1 TemperatureID MaxTemperatureID from [precipio.snapshot.readings].dbo.[Temperature] with(nolock) order by TemperatureID desc'
    MaxTemperatureID<-unlist(SubmitPrecipioQuery(MaxTemperatureIDQuery))
    FileName_TemperatureFirstMsgData_Archive<-file.path(CONFIG_DataWarehouseDir,paste(DataWarehousePrefix,'_FirstMsgArchive_Temperature_',MaxTemperatureID,'.csv',sep=''))
    FirstMsg_TemperatureQuery<-paste('select D.DeviceID, Min(ReadingDate) FirstMsg_Temperature from [precipio.snapshot.readings].dbo.[Temperature] T with(nolock)
                                       join [precipio.snapshot.accounts].dbo.Sensor S on T.SensorID = S.SensorID 
                                   join [precipio.snapshot.accounts].dbo.Device D on S.DeviceID = D.DeviceID where D.AccountID =',CONFIG_AccountID,'and
                                   D.DateDeleted is NULL group by D.DeviceID')
    FirstMsg_TemperatureDF<-SubmitPrecipioQuery(FirstMsg_TemperatureQuery)
    if(nrow(FirstMsg_TemperatureDF)>0){
      
      FirstMsg_TemperatureDF$StrDeviceID<-paste('Device:',FirstMsg_TemperatureDF$DeviceID,sep='')
      FirstMsg_TemperatureDF$FirstMsgDate_Temperature<-substr(FirstMsg_TemperatureDF$FirstMsg_Temperature,1,10)
      ####  Merge the new with the old
      FirstMsg_TemperatureDF_Full<-merge(FirstMsg_TemperatureDF[,c('StrDeviceID','FirstMsgDate_Temperature')],
                                         FirstMsg_TemperatureDF_Archive[,c('StrDeviceID','FirstMsgDate_Temperature')],
                                         by='StrDeviceID',
                                         suffixes=c('NewReadings','ArchivedReadings'),all=TRUE)
      FirstMsg_TemperatureDF_Full$FirstMsgDate_Temperature<-pmin(FirstMsg_TemperatureDF_Full$FirstMsgDate_TemperatureNewReadings,
                                                                 FirstMsg_TemperatureDF_Full$FirstMsgDate_TemperatureArchivedReadings,na.rm = TRUE)
      write.csv(FirstMsg_TemperatureDF_Full[,c('StrDeviceID','FirstMsgDate_Temperature')],file=FileName_TemperatureFirstMsgData_Archive,row.names=FALSE)
    }
    ######  End of Temperature
    
    
    ########  Error Log
    FirstMsg_ErrorLog_ArchiveFilesList<-dir(CONFIG_DataWarehouseDir,pattern = paste(DataWarehousePrefix,'_FirstMsgArchive_ErrorLog_.*csv',sep=''))
    if(length(FirstMsg_ErrorLog_ArchiveFilesList)==0){
      FirstMsg_ErrorLogQuery_Archive<-paste('select EL.DeviceID, Min(ErrorDate) FirstMsg_ErrorLog from [precipio.archive].dbo.ErrorLog EL with(nolock)
  join [precipio.snapshot.accounts].dbo.Device D on EL.DeviceID = D.DeviceID where D.AccountID =',CONFIG_AccountID,'and
    D.DateDeleted is NULL group by EL.DeviceID')
      FirstMsg_ErrorLog1DF_Archive<-SubmitPrecipioQuery(FirstMsg_ErrorLogQuery_Archive)
      FirstMsg_ErrorLog2Query_Archive<-paste('select EL.DeviceID, Min(ErrorDate) FirstMsg_ErrorLog from [precipio.archive].dbo.ErrorLog2 EL with(nolock)
    join [precipio.snapshot.accounts].dbo.Device D on EL.DeviceID = D.DeviceID where D.AccountID =',CONFIG_AccountID,'and
    D.DateDeleted is NULL group by EL.DeviceID',sep='')
      FirstMsg_ErrorLog2DF_Archive<-SubmitPrecipioQuery(FirstMsg_ErrorLog2Query_Archive)
      FirstMsg_ErrorLogDFFull_Archive<-data.table(rbind(FirstMsg_ErrorLog1DF_Archive,FirstMsg_ErrorLog2DF_Archive))
      setkey(FirstMsg_ErrorLogDFFull_Archive,DeviceID)
      FirstMsg_ErrorLogDF_Archive<-FirstMsg_ErrorLogDFFull_Archive[,.(FirstMsg_ErrorLog=min(FirstMsg_ErrorLog)),by=DeviceID]
      
      FirstMsg_ErrorLogDF_Archive$StrDeviceID<-paste('Device:',FirstMsg_ErrorLogDF_Archive$DeviceID,sep='')
      FirstMsg_ErrorLogDF_Archive$FirstMsgDate_ErrorLog<-substr(FirstMsg_ErrorLogDF_Archive$FirstMsg_ErrorLog,1,10)
      MaxErrorLogIDQuery<-'select top 1 ErrorLogID MaxErrorLogID from [precipio.archive].dbo.[ErrorLog2] with(nolock) order by ErrorLogID desc'
      MaxErrorLogID_Archive<-unlist(SubmitPrecipioQuery(MaxErrorLogIDQuery))
      FileName_ErrorLogFirstMsgData_Archive<-file.path(CONFIG_DataWarehouseDir,paste(DataWarehousePrefix,'_FirstMsgArchive_ErrorLog_',MaxErrorLogID_Archive,'.csv',sep=''))
      write.csv(FirstMsg_ErrorLogDF_Archive[,.(StrDeviceID,FirstMsgDate_ErrorLog)],file=FileName_ErrorLogFirstMsgData_Archive,row.names=FALSE)
    }
    FirstMsg_ErrorLog_ArchiveFilesList<-dir(CONFIG_DataWarehouseDir,pattern = paste(DataWarehousePrefix,'_FirstMsgArchive_ErrorLog_.*csv',sep=''))
    ###  If there is more than one file, take the last in the last - very roughly should be the most recent
    FirstMsg_ErrorLog_ArchiveFile<-file.path(CONFIG_DataWarehouseDir,FirstMsg_ErrorLog_ArchiveFilesList[length(FirstMsg_ErrorLog_ArchiveFilesList)])
    FirstMsg_ErrorLogDF_Archive<-read.csv(FirstMsg_ErrorLog_ArchiveFile,stringsAsFactors = FALSE,header=TRUE)
    ###  Extract MaxID
    ###  Remove the basic file name and the extension
    MaxErrorLogID_Archive<-gsub(paste(DataWarehousePrefix,'_FirstMsgArchive_ErrorLog_',sep=''),'',gsub('.csv','',basename(FirstMsg_ErrorLog_ArchiveFile)))
    
    MaxErrorLogIDQuery<-'select top 1 ErrorLogID MaxErrorLogID from [precipio.snapshot.readings].dbo.[ErrorLog2] P with(nolock) order by ErrorLogID desc'
    MaxErrorLogID<-unlist(SubmitPrecipioQuery(MaxErrorLogIDQuery))
    FileName_ErrorLogFirstMsgData_Archive<-file.path(CONFIG_DataWarehouseDir,paste(DataWarehousePrefix,'_FirstMsgArchive_ErrorLog_',MaxErrorLogID,'.csv',sep=''))
    
    ####  Incremental query
    FirstMsg_ErrorLogQuery<-paste('select EL.DeviceID, Min(ErrorDate) FirstMsg_ErrorLog from [precipio.snapshot.readings].dbo.ErrorLog2 EL with(nolock)
                              join [precipio.snapshot.accounts].dbo.Device D on EL.DeviceID = D.DeviceID where D.AccountID =',CONFIG_AccountID,'and D.DateDeleted is NULL and EL.ErrorLogID >',MaxErrorLogID_Archive,'group by EL.DeviceID')
    FirstMsg_ErrorLogDF<-SubmitPrecipioQuery(FirstMsg_ErrorLogQuery)
    if(nrow(FirstMsg_ErrorLogDF)>0){
      
      FirstMsg_ErrorLogDF$StrDeviceID<-paste('Device:',FirstMsg_ErrorLogDF$DeviceID,sep='')
      FirstMsg_ErrorLogDF$FirstMsgDate_ErrorLog<-substr(FirstMsg_ErrorLogDF$FirstMsg_ErrorLog,1,10)
      ####  Merge the new with the old
      FirstMsg_ErrorLogDF_Full<-merge(FirstMsg_ErrorLogDF[,c('StrDeviceID','FirstMsgDate_ErrorLog')],
                                      FirstMsg_ErrorLogDF_Archive[,c('StrDeviceID','FirstMsgDate_ErrorLog')],
                                      by='StrDeviceID',
                                      suffixes=c('NewReadings','ArchivedReadings'),all=TRUE)
      FirstMsg_ErrorLogDF_Full$FirstMsgDate_ErrorLog<-pmin(FirstMsg_ErrorLogDF_Full$FirstMsgDate_ErrorLogNewReadings,
                                                           FirstMsg_ErrorLogDF_Full$FirstMsgDate_ErrorLogArchivedReadings,na.rm = TRUE)
      write.csv(FirstMsg_ErrorLogDF_Full[,c('StrDeviceID','FirstMsgDate_ErrorLog')],file=FileName_ErrorLogFirstMsgData_Archive,row.names=FALSE)
    }
    

    PowerFirstMsgTable<-read.csv(FileName_PowerFirstMsgData_Archive,header=TRUE,stringsAsFactors = FALSE)
    TemperatureFirstMsgTable<-read.csv(FileName_TemperatureFirstMsgData_Archive,header=TRUE,stringsAsFactors = FALSE)
    ErrorLogFirstMsgTable<-read.csv(FileName_ErrorLogFirstMsgData_Archive,header=TRUE,stringsAsFactors = FALSE)
    ErrorLogFirstMsgTable<-sqldf('select StrDeviceID, min(FirstMsgDate_ErrorLog) FirstMsgDate_ErrorLog from ErrorLogFirstMsgTable group by StrDeviceID')
    PowerTemp_FirstMsgTable<-merge(PowerFirstMsgTable,TemperatureFirstMsgTable,by='StrDeviceID',all=TRUE)
    FirstMsgTable<-merge(PowerTemp_FirstMsgTable,ErrorLogFirstMsgTable,by='StrDeviceID',all=TRUE)
    FirstMsgColumns<-which(grepl('FirstMsgDate_',names(FirstMsgTable)))
    FirstMsgTable$DeviceInstallDate<-apply(FirstMsgTable[,FirstMsgColumns],1,function(x){min(x,na.rm=TRUE)})
    NAIndices<-which(is.na(FirstMsgTable$DeviceInstallDate))
    for(NAIndex in 1:length(NAIndices)){
      thisNAIndex<-NAIndices[NAIndex]
      thisNonNACols<-which(!is.na(FirstMsgTable[thisNAIndex,FirstMsgColumns]))
      if(length(thisNonNACols)>0)
        FirstMsgTable$DeviceInstallDate[thisNAIndex]<-min(as.character(FirstMsgTable[thisNAIndex,FirstMsgColumns[thisNonNACols]]))
    }
    print(paste('Writing FirstMsgTable to file:',FileName_DeviceInstallDB))
    write.csv(FirstMsgTable,file=FileName_DeviceInstallDB,row.names=FALSE)
    

    ####  Need to take min date from Device table or first Reading date - found a case in CVS where DeviceTable version is not oldest
    FirstMsgMergeDF<-data.frame(StrDeviceID=FirstMsgTable$StrDeviceID,FirstMsgDate=FirstMsgTable$DeviceInstallDate,stringsAsFactors = FALSE)
    DeviceAndStoreDF_Base<-merge(DeviceAndStoreDF_Base,FirstMsgMergeDF,all.x=TRUE,by='StrDeviceID')
    ##  Need this to avoid the NA's from causing issues later
    # browser()
    DeviceAndStoreDF_Base$FirstMsgDate<-ifelse(is.na(DeviceAndStoreDF_Base$FirstMsgDate),DeviceAndStoreDF_Base$DeviceInstallDate,DeviceAndStoreDF_Base$FirstMsgDate)
    DeviceAndStoreDF_Base$DeviceFirstActivity<-pmin(as.Date(as.character(DeviceAndStoreDF_Base$DeviceInstallDate),format='%Y-%m-%d %H:%M:%S'),
                                                    as.Date(as.character(DeviceAndStoreDF_Base$FirstMsgDate),format='%Y-%m-%d %H:%M:%S'),na.rm = TRUE)
    
    ####  Now update for the devices that didn't have their InstallDate in the DB:
    DeviceInstallDateUpdates<-NULL
    NumDevicesWithNoInstallDate<-length(DevicesNeverInstalled)
    for(thisDeviceIter in 1:NumDevicesWithNoInstallDate){
      thisStrDeviceID<-DevicesNeverInstalled[thisDeviceIter]
      print(paste('GenDeviceAndStoreInfoDW::   processing device',thisDeviceIter,'of',NumDevicesWithNoInstallDate, 'with missing install information, StrDeviceID:',thisStrDeviceID))
#       if(thisStrDeviceID=='Device:11666000000121484047')
#       {
#         browser()
#       }
      thisDeviceID<-sub('Device:','',thisStrDeviceID)
      thisDeviceInstallDate<-FirstMsgTable$DeviceInstallDate[which(FirstMsgTable$StrDeviceID==thisStrDeviceID)]
      thisDeviceInstallDate<-ifelse(length(thisDeviceInstallDate)==0,'NO DATA',thisDeviceInstallDate)
      DeviceAndStoreDF_Base$DeviceInstallDate[which(DeviceAndStoreDF_Base$StrDeviceID==thisStrDeviceID)]<-thisDeviceInstallDate
      DeviceInstallDateUpdates<-rbind(DeviceInstallDateUpdates,data.frame(DeviceID=thisDeviceID,StrDeviceID=thisStrDeviceID,DeviceInstallDate=thisDeviceInstallDate))
    }
    write.csv(DeviceInstallDateUpdates,file=file.path(CONFIG_DataDir,paste(CONFIG_DataWarehousePrefix,'_MissingDeviceInstallEntries.csv')),row.names=FALSE)
  }
  
  
  write.csv(DeviceAndStoreDF_Base,file=Filename_DeviceAndStoreDW,row.names=FALSE)
}
