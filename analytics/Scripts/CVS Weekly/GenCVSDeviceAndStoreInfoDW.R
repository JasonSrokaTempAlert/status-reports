print('Kicking off GenCVSDeviceAndStoreInfoDW.R analysis')
print(paste('Working directory is',getwd()))
source('D:/RStudioLocalHome/.Rprofile')
print('done sourcing .Rprofile')
DataAccessScriptsDir<-file.path(GitHomeDir,'Utilities/Data Access')
ConfigsDir<-file.path(GitHomeDir,'Config')

####  Load other tools
source(file.path(DataAccessScriptsDir,'DevSnapshotAccess.R'))

####  Load 'config' info - that will be shared across scripts
##  This one sets key directory paths and Data Warehouse file names
source(file.path(ConfigsDir,'CVS_Reporting_EnvConfig.R'))

####  libraries
library(sqldf)
library(plyr)

####  File Names
Filename_StoreInfoDB<-file.path(CONFIG_DataDir,paste(DataWarehousePrefix,'StoreAndInstallInfo.csv',sep=''))
FileName_DeviceInstallDB<-file.path(CONFIG_DataDir,'DeviceInstallationDatesDB.csv')


# #####   Store Info DB generation
StoreInfoQuery<-'select * from [reporting.data].dbo.cvsdata CVSData with (nolock)'
StoreInfoDF<-SubmitPrecipioQuery(StoreInfoQuery)
####  Additional Query for stores not in table - e.g. target stores
AllCVSGroupsQuery<-"select * from [precipio.snapshot.accounts].dbo.[Group] G with(nolock) 
where G.AccountID = 4226 and DateDeleted is NULL and DisplayName like 'Store%' and GroupID not in (11946,8984,8985)"
AllCVSGroupsDF<-SubmitPrecipioQuery(AllCVSGroupsQuery)
ExtraCVSGroupsRawDF<-AllCVSGroupsDF[which(!(AllCVSGroupsDF$GroupID %in% StoreInfoDF$storegroupid)),]
write.csv(ExtraCVSGroupsRawDF,file='D:/Analytics/CVS/MissingCVSStores.csv')
ExtraCVSGroupsForMerge<-data.frame(storename=ExtraCVSGroupsRawDF$DisplayName,storegroupid=ExtraCVSGroupsRawDF$GroupID)
ExtraCVSGroupsForMerge$storename<-as.character(ExtraCVSGroupsForMerge$storename)
ExtractStoreNumFromDisplayName<-function(DisplayName){
  # print(paste('ExtractStoreNumFromDisplayName()::  call with string',DisplayName))
  # browser()
  FirstSplit<-strsplit(DisplayName,'#',fixed=TRUE)[[1]][2]
  # print('FirstSplit')
  # print(FirstSplit)
  SecondSplit<-strsplit(FirstSplit,' -',fixed=TRUE)[[1]][1]
  # print('SecondSplit')
  # print(SecondSplit)
  return(SecondSplit)
}
ExtraCVSGroupsForMerge$storeid<-sapply(ExtraCVSGroupsForMerge$storename,ExtractStoreNumFromDisplayName)
StoreInfoDF<-rbind.fill(StoreInfoDF,ExtraCVSGroupsForMerge)
StoreInfoDF$storeid<-as.numeric(StoreInfoDF$storeid)

StoreInstallDatesQuery<-"select g.DisplayName, min(a.ActionDate) StoreInstallDateTime, g.GroupID GroupId from [precipio.snapshot.accounts].dbo.device d with(nolock)
inner join (select substring(action, 13, 20) as did, actiondate from [precipio.snapshot.accounts].dbo.auditlog with(nolock) where userid in (8148, 8149) and ItemType = 'apiV3 - DeviceUpsert') a
on (convert(varchar(20), d.deviceid) = a.did)
inner join [precipio.snapshot.accounts].dbo.[group] g with(nolock) on (d.groupid = g.groupid)
and d.AccountID = 4226
group by g.DisplayName, g.GroupID
order by g.DisplayName"
StoreInstallDatesDF<-SubmitPrecipioQuery(StoreInstallDatesQuery)   
####  Remove RMA
StoreGroupIdsToIgnore<-StoreInstallDatesDF$GroupId[which((!grepl('Store #',StoreInstallDatesDF$DisplayName,fixed=TRUE)) | grepl('#9999[89]',StoreInstallDatesDF$DisplayName))]
StoreInstallDatesDF<-StoreInstallDatesDF[-which(StoreInstallDatesDF$GroupId %in% StoreGroupIdsToIgnore),]
StoreInstallDatesDF$StoreInstallDate<-substr(as.character(StoreInstallDatesDF$StoreInstallDateTime),1,10)
InstalledStoresGroupIDsList<-unique(StoreInstallDatesDF$GroupId[which(grepl('Store #',StoreInstallDatesDF$DisplayName) & !grepl('#9999[89]',StoreInstallDatesDF$DisplayName))])

StoreInfoDF<-merge(StoreInfoDF,StoreInstallDatesDF[,c('GroupId','StoreInstallDate')],by.x='storegroupid',by.y='GroupId',all=TRUE)
# ####  Add in square footage info
# Filename_CVSStoreInfoTable<-'D:/Analytics/CVS/CVS Info/CVS Store Info.csv'
# CVSStoreInfoTable<-read.csv(Filename_CVSStoreInfoTable,header=TRUE,stringsAsFactors = TRUE)
# CVSStoreInfoTable$Retail.SF<-as.numeric(CVSStoreInfoTable$Retail.SF)
# StoreInfoDF$RetailSquareFootage<-sapply(StoreInfoDF$storeid,function(x) CVSStoreInfoTable$Retail.SF[which(CVSStoreInfoTable$Store.Nbr==x)])
# write.csv(StoreInfoDF,file=Filename_StoreInfoDB,row.names=FALSE)

######     Device Info DF
DeviceInfoQuery<-paste('select DeviceID, NickName DeviceLabel, D.Firmware, D.DateDeleted, Latitude DeviceLat, Longitude DeviceLong, LastActivity, D.GroupId,
                       G.DisplayName GroupName from [precipio.snapshot.accounts].dbo.Device D with(nolock) join [precipio.snapshot.accounts].dbo.[Group] G with(nolock) on D.GroupID = G.GroupID
                       where D.AccountID = 4226 and  D.DateDeleted is NULL and D.GroupID is not null and D.GroupId not in (',paste(StoreGroupIdsToIgnore,collapse=','),')')
DeviceInfoDF<-SubmitPrecipioQuery(DeviceInfoQuery)
DeviceInfoDF$StrDeviceID<-paste('Device:',DeviceInfoDF$DeviceID,sep='')
DeviceInfoDF$NodeLocation<-as.character(DeviceInfoDF$DeviceLabel)
DeviceInfoDF$NodeLocation<-ifelse(grepl('RX',DeviceInfoDF$NodeLocation,ignore.case=TRUE) & !grepl('Gateway',DeviceInfoDF$NodeLocation,ignore.case=TRUE),'Pharmacy',DeviceInfoDF$NodeLocation)
DeviceInfoDF$NodeLocation<-ifelse(grepl('FS',DeviceInfoDF$NodeLocation,ignore.case=TRUE),'FrontStore',DeviceInfoDF$NodeLocation)
DeviceInfoDF$NodeLocation<-ifelse(grepl('MC',DeviceInfoDF$NodeLocation,ignore.case=TRUE),'MinuteClinic',DeviceInfoDF$NodeLocation)
DeviceInfoDF$NodeLocation<-ifelse(grepl('Gateway',DeviceInfoDF$NodeLocation,ignore.case=TRUE) | grepl('Station',DeviceInfoDF$NodeLocation,ignore.case=TRUE),'Gateway',DeviceInfoDF$NodeLocation)
DeviceInfoDF$NodeLocation<-ifelse(grepl('Default',DeviceInfoDF$NodeLocation,ignore.case=TRUE),'Default Type',DeviceInfoDF$NodeLocation)
DeviceInfoDF$NodeLocation<-ifelse(grepl('Generic',DeviceInfoDF$NodeLocation,ignore.case=TRUE),'Generic Fridge',DeviceInfoDF$NodeLocation)
DeviceInfoDF$NodeLocation<-ifelse(grepl('Ambient',DeviceInfoDF$NodeLocation,ignore.case=TRUE),'Pharmacy',DeviceInfoDF$NodeLocation)
# write.csv(DeviceInfoDF,file=Filename_DeviceInfoDF,row.names = FALSE)


DeviceAndStoreDF_Base<-merge(StoreInfoDF,DeviceInfoDF,by.x='storegroupid',by.y='GroupId',all.y=TRUE)
write.csv(DeviceAndStoreDF_Base,file=Filename_DeviceAndStoreDW_OldFields,row.names=FALSE)
# DeviceAndStoreDF_Base<-read.csv(file=Filename_DeviceAndStoreDW_OldFields,header=TRUE)




StoreIDsList<-unique(DeviceAndStoreDF_Base$storeid)
StoreIDsList<-StoreIDsList[!is.na(StoreIDsList)]
# StoreIDsList<-c(13,1086, 4796, 212, 1414, 397, 8320, 971, 493, 15003, 645, 670, 2234, 729, 2049,10551, 2417, 326, 590)

GroupIDsList<-sapply(StoreIDsList,
                     function(x){
                       MatchingStoreIndices<-which(DeviceAndStoreDF_Base$storeid==x)
                       if(length(MatchingStoreIndices)==0) {
                         browser()
                         return(NA)
                       } else DeviceAndStoreDF_Base$storegroupid[min(MatchingStoreIndices)]})
GroupIDsList<-InstalledStoresGroupIDsList
PowerEntriesDF<-NULL
NumGroupIDs<-length(GroupIDsList)

GenSQLDateCondition<-function(Date,SQLDateField='ReadingDate',DateComparison=' > '){
  ifelse(Date=='',Date,paste(' and ',SQLDateField,DateComparison,"'",Date,"'",sep=''))
}

####   First Messages checks
####
####  Using the pattern of a set of data in Archive and a set of newer data in snapshot
FileName_TemperatureFirstMsgData<-file.path(CONFIG_DataWarehouseDir,'TemperatureFirstMsgData.csv')
FileName_TemperatureFirstMsgData_Archive<-file.path(CONFIG_DataWarehouseDir,'TemperatureFirstMsgData_Archive.csv')
FileName_PowerFirstMsgData<-file.path(CONFIG_DataWarehouseDir,'PowerFirstMsgData.csv')
FileName_PowerFirstMsgData_Archive<-file.path(CONFIG_DataWarehouseDir,'PowerFirstMsgData_Archive.csv')
FileName_ErrorLogFirstMsgData<-file.path(CONFIG_DataWarehouseDir,'ErrorLogFirstMsgData.csv')
FileName_ErrorLogFirstMsgData_Archive<-file.path(CONFIG_DataWarehouseDir,'ErrorLogFirstMsgData_Archive.csv')

########  Power
####  Check for existing processed data
FirstMsg_Power_ArchiveFilesList<-dir(CONFIG_DataWarehouseDir,pattern = 'CVS_FirstMsgArchive_Power_.*csv')
if(length(FirstMsg_Power_ArchiveFilesList)==0){
  FirstMsg_PowerQuery_Archive<-'select P.DeviceID, Min(ReadingDate) FirstMsg_Power from [precipio.archive].dbo.[Power] P with(nolock)
  join [precipio.snapshot.accounts].dbo.Device D on P.DeviceID = D.DeviceID where D.AccountID = 4226 and
  D.DateDeleted is NULL group by P.DeviceID'
  FirstMsg_PowerDF_Archive<-SubmitPrecipioQuery(FirstMsg_PowerQuery_Archive)
  FirstMsg_PowerDF_Archive$StrDeviceID<-paste('Device:',FirstMsg_PowerDF_Archive$DeviceID,sep='')
  FirstMsg_PowerDF_Archive$FirstMsgDate_Power<-substr(FirstMsg_PowerDF_Archive$FirstMsg_Power,1,10)
  MaxPowerIDQuery<-'select top 1 PowerID MaxPowerID from [precipio.archive].dbo.[Power] P with(nolock) order by PowerID desc'
  MaxPowerID<-unlist(SubmitPrecipioQuery(MaxPowerIDQuery))
  FileName_PowerFirstMsgData_Archive<-file.path(CONFIG_DataWarehouseDir,paste('CVS_FirstMsgArchive_Power_',MaxPowerID,'.csv',sep=''))
  write.csv(FirstMsg_PowerDF_Archive[,!names(FirstMsg_PowerDF_Archive) == 'DeviceID'],file=FileName_PowerFirstMsgData_Archive,row.names=FALSE)
}
FirstMsg_Power_ArchiveFilesList<-dir(CONFIG_DataWarehouseDir,pattern = 'CVS_FirstMsgArchive_Power_.*csv')
###  If there is more than one file, take the last in the last - very roughly should be the most recent
FirstMsg_Power_ArchiveFile<-file.path(CONFIG_DataWarehouseDir,FirstMsg_Power_ArchiveFilesList[length(FirstMsg_Power_ArchiveFilesList)])
FirstMsg_PowerDF_Archive<-read.csv(FirstMsg_Power_ArchiveFile,header=TRUE,stringsAsFactors = FALSE)
###  Extract MaxID
###  Remove the basic file name and the extension
MaxPowerID_Archive<-gsub('CVS_FirstMsgArchive_Power_','',gsub('.csv','',basename(FirstMsg_Power_ArchiveFile)))

####  Now do the incremental query
MaxPowerIDQuery<-'select top 1 PowerID MaxPowerID from [precipio.snapshot.readings].dbo.[Power] P with(nolock) order by PowerID desc'
MaxPowerID<-unlist(SubmitPrecipioQuery(MaxPowerIDQuery))
FileName_PowerFirstMsgData_Archive<-file.path(CONFIG_DataWarehouseDir,paste('CVS_FirstMsgArchive_Power_',MaxPowerID,'.csv',sep=''))
FirstMsg_PowerQuery<-paste('select P.DeviceID, Min(ReadingDate) FirstMsg_Power from [precipio.snapshot.readings].dbo.[Power] P with(nolock)
                           join [precipio.snapshot.accounts].dbo.Device D on P.DeviceID = D.DeviceID where D.AccountID = 4226 and D.DateDeleted is NULL and P.PowerID >',MaxPowerID_Archive,'group by P.DeviceID')
FirstMsg_PowerDF<-SubmitPrecipioQuery(FirstMsg_PowerQuery)
if(nrow(FirstMsg_PowerDF)>0){
  
FirstMsg_PowerDF$StrDeviceID<-paste('Device:',FirstMsg_PowerDF$DeviceID,sep='')
FirstMsg_PowerDF$FirstMsgDate_Power<-substr(FirstMsg_PowerDF$FirstMsg_Power,1,10)
####  Merge the new with the old
FirstMsg_PowerDF_Full<-merge(FirstMsg_PowerDF[,c('StrDeviceID','FirstMsgDate_Power')],
                             FirstMsg_PowerDF_Archive[,c('StrDeviceID','FirstMsgDate_Power')],
                             by='StrDeviceID',
                             suffixes=c('NewReadings','ArchivedReadings'),all=TRUE)
FirstMsg_PowerDF_Full$FirstMsgDate_Power<-pmin(FirstMsg_PowerDF_Full$FirstMsgDate_PowerNewReadings,
                                               FirstMsg_PowerDF_Full$FirstMsgDate_PowerArchivedReadings,na.rm = TRUE)
write.csv(FirstMsg_PowerDF_Full[,c('StrDeviceID','FirstMsgDate_Power')],file=FileName_PowerFirstMsgData_Archive,row.names=FALSE)
}

#########   Temperature
####  Check for existing processed data
FirstMsg_Temperature_ArchiveFilesList<-dir(CONFIG_DataWarehouseDir,pattern = 'CVS_FirstMsgArchive_Temperature_.*csv')
if(length(FirstMsg_Temperature_ArchiveFilesList)==0){
  FirstMsg_TemperatureQuery_Archive<-'select D.DeviceID, Min(ReadingDate) FirstMsg_Temperature from [precipio.archive].dbo.Temperature T with(nolock)
  join [precipio.snapshot.accounts].dbo.Sensor S with(nolock) on S.SensorID = T.SensorID join [precipio.snapshot.accounts].dbo.Device D with(nolock) on S.DeviceID = D.DeviceID 
  where D.AccountID = 4226 and D.DateDeleted is NULL group by D.DeviceID'
  FirstMsg_TemperatureDF_Archive<-SubmitPrecipioQuery(FirstMsg_TemperatureQuery_Archive)
  FirstMsg_TemperatureDF_Archive$StrDeviceID<-paste('Device:',FirstMsg_TemperatureDF_Archive$DeviceID,sep='')
  FirstMsg_TemperatureDF_Archive$FirstMsgDate_Temperature<-substr(FirstMsg_TemperatureDF_Archive$FirstMsg_Temperature,1,10)
  MaxTemperatureIDQuery<-'select top 1 TemperatureID MaxTemperatureID from [precipio.archive].dbo.[Temperature] T with(nolock) order by TemperatureID desc'
  MaxTemperatureID_Archive<-unlist(SubmitPrecipioQuery(MaxTemperatureIDQuery))
  FileName_TemperatureFirstMsgData_Archive<-file.path(CONFIG_DataWarehouseDir,paste('CVS_FirstMsgArchive_Temperature_',MaxTemperatureID_Archive,'.csv',sep=''))
  write.csv(FirstMsg_TemperatureDF_Archive[,!names(FirstMsg_TemperatureDF_Archive) == 'DeviceID'],file=FileName_TemperatureFirstMsgData_Archive,row.names=FALSE)
}
FirstMsg_Temperature_ArchiveFilesList<-dir(CONFIG_DataWarehouseDir,pattern = 'CVS_FirstMsgArchive_Temperature_.*csv')
###  If there is more than one file, take the last in the last - very roughly should be the most recent
FirstMsg_Temperature_ArchiveFile<-file.path(CONFIG_DataWarehouseDir,FirstMsg_Temperature_ArchiveFilesList[length(FirstMsg_Temperature_ArchiveFilesList)])
FirstMsg_TemperatureDF_Archive<-read.csv(FirstMsg_Temperature_ArchiveFile,stringsAsFactors = FALSE)
###  Extract MaxID
###  Remove the basic file name and the extension
MaxTemperatureID_Archive<-gsub('CVS_FirstMsgArchive_Temperature_','',gsub('.csv','',basename(FirstMsg_Temperature_ArchiveFile)))

MaxTemperatureIDQuery<-'select top 1 TemperatureID MaxTemperatureID from [precipio.snapshot.readings].dbo.[Temperature] T with(nolock) order by TemperatureID desc'
MaxTemperatureID<-unlist(SubmitPrecipioQuery(MaxTemperatureIDQuery))
FileName_TemperatureFirstMsgData<-file.path(CONFIG_DataWarehouseDir,paste('CVS_FirstMsgArchive_Temperature_',MaxTemperatureID,'.csv',sep=''))

####  Get new data
FirstMsg_TemperatureQuery<-paste('select D.DeviceID, Min(ReadingDate) FirstMsg_Temperature from [precipio.snapshot.readings].dbo.Temperature T with(nolock)
                                 join [precipio.snapshot.accounts].dbo.Sensor S with(nolock) on S.SensorID = T.SensorID join [precipio.snapshot.accounts].dbo.Device D with(nolock) on S.DeviceID = D.DeviceID 
                                 where D.AccountID = 4226 and D.DateDeleted is NULL and TemperatureID >',MaxTemperatureID_Archive,'group by D.DeviceID')
FirstMsg_TemperatureDF<-SubmitPrecipioQuery(FirstMsg_TemperatureQuery)
if(nrow(FirstMsg_TemperatureDF)>0){
FirstMsg_TemperatureDF$StrDeviceID<-paste('Device:',FirstMsg_TemperatureDF$DeviceID,sep='')
FirstMsg_TemperatureDF$FirstMsgDate_Temperature<-substr(FirstMsg_TemperatureDF$FirstMsg_Temperature,1,10)
####  Merge the new with the old
FirstMsg_TemperatureDF_Full<-merge(FirstMsg_TemperatureDF[,c('StrDeviceID','FirstMsgDate_Temperature')],
                                   FirstMsg_TemperatureDF_Archive[,c('StrDeviceID','FirstMsgDate_Temperature')],
                                   by='StrDeviceID',
                                   suffixes=c('NewReadings','ArchivedReadings'),all=TRUE)
FirstMsg_TemperatureDF_Full$FirstMsgDate_Temperature<-pmin(FirstMsg_TemperatureDF_Full$FirstMsgDate_TemperatureNewReadings,
                                                           FirstMsg_TemperatureDF_Full$FirstMsgDate_TemperatureArchivedReadings,na.rm = TRUE)
####  Store the result
write.csv(FirstMsg_TemperatureDF[,!names(FirstMsg_TemperatureDF) == 'DeviceID'],file=FileName_TemperatureFirstMsgData,row.names=FALSE)
}

########  Error Log
FirstMsg_ErrorLog_ArchiveFilesList<-dir(CONFIG_DataWarehouseDir,pattern = 'CVS_FirstMsgArchive_ErrorLog_.*csv')
if(length(FirstMsg_ErrorLog_ArchiveFilesList)==0){
  FirstMsg_ErrorLogQuery_Archive<-'select EL.DeviceID, Min(ErrorDate) FirstMsg_ErrorLog from [precipio.archive].dbo.ErrorLog EL with(nolock)
  join [precipio.snapshot.accounts].dbo.Device D on EL.DeviceID = D.DeviceID where D.AccountID = 4226 and
  D.DateDeleted is NULL group by EL.DeviceID'
  FirstMsg_ErrorLog1DF_Archive<-SubmitPrecipioQuery(FirstMsg_ErrorLogQuery_Archive)
  FirstMsg_ErrorLog2Query_Archive<-'select EL.DeviceID, Min(ErrorDate) FirstMsg_ErrorLog from [precipio.archive].dbo.ErrorLog2 EL with(nolock)
  join [precipio.snapshot.accounts].dbo.Device D on EL.DeviceID = D.DeviceID where D.AccountID = 4226 and
  D.DateDeleted is NULL group by EL.DeviceID'
  FirstMsg_ErrorLog2DF_Archive<-SubmitPrecipioQuery(FirstMsg_ErrorLog2Query_Archive)
  FirstMsg_ErrorLogDFFull_Archive<-data.table(rbind(FirstMsg_ErrorLog1DF_Archive,FirstMsg_ErrorLog2DF_Archive))
  setkey(FirstMsg_ErrorLogDFFull_Archive,DeviceID)
  FirstMsg_ErrorLogDF_Archive<-FirstMsg_ErrorLogDFFull_Archive[,.(FirstMsg_ErrorLog=min(FirstMsg_ErrorLog)),by=DeviceID]
  
  FirstMsg_ErrorLogDF_Archive$StrDeviceID<-paste('Device:',FirstMsg_ErrorLogDF_Archive$DeviceID,sep='')
  FirstMsg_ErrorLogDF_Archive$FirstMsgDate_ErrorLog<-substr(FirstMsg_ErrorLogDF_Archive$FirstMsg_ErrorLog,1,10)
  MaxErrorLogIDQuery<-'select top 1 ErrorLogID MaxErrorLogID from [precipio.archive].dbo.[ErrorLog2] with(nolock) order by ErrorLogID desc'
  MaxErrorLogID_Archive<-unlist(SubmitPrecipioQuery(MaxErrorLogIDQuery))
  FileName_ErrorLogFirstMsgData_Archive<-file.path(CONFIG_DataWarehouseDir,paste('CVS_FirstMsgArchive_ErrorLog_',MaxErrorLogID_Archive,'.csv',sep=''))
  write.csv(FirstMsg_ErrorLogDF_Archive[,.(StrDeviceID,FirstMsgDate_ErrorLog)],file=FileName_ErrorLogFirstMsgData_Archive,row.names=FALSE)
}
FirstMsg_ErrorLog_ArchiveFilesList<-dir(CONFIG_DataWarehouseDir,pattern = 'CVS_FirstMsgArchive_ErrorLog_.*csv')
###  If there is more than one file, take the last in the last - very roughly should be the most recent
FirstMsg_ErrorLog_ArchiveFile<-file.path(CONFIG_DataWarehouseDir,FirstMsg_ErrorLog_ArchiveFilesList[length(FirstMsg_ErrorLog_ArchiveFilesList)])
FirstMsg_ErrorLogDF_Archive<-read.csv(FirstMsg_ErrorLog_ArchiveFile,stringsAsFactors = FALSE,header=TRUE)
###  Extract MaxID
###  Remove the basic file name and the extension
MaxErrorLogID_Archive<-gsub('CVS_FirstMsgArchive_ErrorLog_','',gsub('.csv','',basename(FirstMsg_ErrorLog_ArchiveFile)))

MaxErrorLogIDQuery<-'select top 1 ErrorLogID MaxErrorLogID from [precipio.snapshot.readings].dbo.[ErrorLog2] P with(nolock) order by ErrorLogID desc'
MaxErrorLogID<-unlist(SubmitPrecipioQuery(MaxErrorLogIDQuery))
FileName_ErrorLogFirstMsgData_Archive<-file.path(CONFIG_DataWarehouseDir,paste('CVS_FirstMsgArchive_ErrorLog_',MaxErrorLogID,'.csv',sep=''))

####  Incremental query
FirstMsg_ErrorLogQuery<-paste('select EL.DeviceID, Min(ErrorDate) FirstMsg_ErrorLog from [precipio.snapshot.readings].dbo.ErrorLog2 EL with(nolock)
                              join [precipio.snapshot.accounts].dbo.Device D on EL.DeviceID = D.DeviceID where D.AccountID = 4226 and D.DateDeleted is NULL and EL.ErrorLogID >',MaxErrorLogID_Archive,'group by EL.DeviceID')
FirstMsg_ErrorLogDF<-SubmitPrecipioQuery(FirstMsg_ErrorLogQuery)
if(nrow(FirstMsg_ErrorLogDF)>0){
  
FirstMsg_ErrorLogDF$StrDeviceID<-paste('Device:',FirstMsg_ErrorLogDF$DeviceID,sep='')
FirstMsg_ErrorLogDF$FirstMsgDate_ErrorLog<-substr(FirstMsg_ErrorLogDF$FirstMsg_ErrorLog,1,10)
####  Merge the new with the old
FirstMsg_ErrorLogDF_Full<-merge(FirstMsg_ErrorLogDF[,c('StrDeviceID','FirstMsgDate_ErrorLog')],
                                FirstMsg_ErrorLogDF_Archive[,c('StrDeviceID','FirstMsgDate_ErrorLog')],
                                by='StrDeviceID',
                                suffixes=c('NewReadings','ArchivedReadings'),all=TRUE)
FirstMsg_ErrorLogDF_Full$FirstMsgDate_ErrorLog<-pmin(FirstMsg_ErrorLogDF_Full$FirstMsgDate_ErrorLogNewReadings,
                                                     FirstMsg_ErrorLogDF_Full$FirstMsgDate_ErrorLogArchivedReadings,na.rm = TRUE)
write.csv(FirstMsg_ErrorLogDF_Full[,c('StrDeviceID','FirstMsgDate_ErrorLog')],file=FileName_ErrorLogFirstMsgData_Archive,row.names=FALSE)
}



FirstMsg_PowerDF_Full<-read.csv(FileName_PowerFirstMsgData_Archive,header=TRUE,stringsAsFactors = FALSE)
FirstMsg_ErrorLogDF_Full<-read.csv(FileName_ErrorLogFirstMsgData_Archive,header=TRUE,stringsAsFactors = FALSE)
FirstMsg_TemperatureDF_Full<-read.csv(FileName_TemperatureFirstMsgData,header=TRUE,stringsAsFactors = FALSE)
# PowerFirstMsgTable<-read.csv(FileName_PowerFirstMsgData,header=TRUE,stringsAsFactors = FALSE)
# TemperatureFirstMsgTable<-read.csv(FileName_TemperatureFirstMsgData,header=TRUE,stringsAsFactors = FALSE)
# ErrorLogFirstMsgTable<-read.csv(FileName_ErrorLogFirstMsgData,header=TRUE,stringsAsFactors = FALSE)
# ErrorLogFirstMsgTable<-sqldf('select StrDeviceID, min(FirstMsgDate_ErrorLog) FirstMsgDate_ErrorLog from ErrorLogFirstMsgTable group by StrDeviceID')
PowerTemp_FirstMsgTable<-merge(FirstMsg_PowerDF_Full[,c('StrDeviceID','FirstMsgDate_Power')],FirstMsg_TemperatureDF_Full[,c('StrDeviceID','FirstMsgDate_Temperature')],by='StrDeviceID',all=TRUE)
FirstMsgTable<-merge(PowerTemp_FirstMsgTable,FirstMsg_ErrorLogDF_Full[,c('StrDeviceID','FirstMsgDate_ErrorLog')],by='StrDeviceID',all=TRUE)
FirstMsgColumns<-which(grepl('FirstMsgDate_',names(FirstMsgTable)))
FirstMsgTable$DeviceInstallDate<-apply(FirstMsgTable[,FirstMsgColumns],1,function(x) min(x,na.rm=TRUE))
# NAIndices<-which(is.na(FirstMsgTable$DeviceInstallDate))
# for(NAIndex in 1:length(NAIndices)){
#   thisNAIndex<-NAIndices[NAIndex]
#   thisNonNACols<-which(!is.na(FirstMsgTable[thisNAIndex,FirstMsgColumns]))
#   if(length(thisNonNACols)>0)
#     FirstMsgTable$DeviceInstallDate[thisNAIndex]<-min(as.character(FirstMsgTable[thisNAIndex,FirstMsgColumns[thisNonNACols]]))
# }
write.csv(FirstMsgTable,file=FileName_DeviceInstallDB,row.names=FALSE)


# ####  This re-creates with each run a SQL table that has the Device Install dates - originally requested by Sally
# SimpleDeviceInstallTable<-data.frame(DeviceID = substr(FirstMsgTable$StrDeviceID,8,27),DeviceInstallDate = FirstMsgTable$DeviceInstallDate)
# DropTableQuery<-'DROP TABLE [reporting.data].dbo.DeviceInstallDates'
# TableDropResponse<-SubmitPrecipioQuery(DropTableQuery)
# print(paste('Table Drop Response:',TableDropResponse))
# DeviceDateTableCreateQuery<-'CREATE TABLE [reporting.data].dbo.DeviceInstallDates ( DeviceID decimal(20,0), DeviceInstallDate date,PRIMARY KEY (DeviceID))'
# TableCreateResponse<-SubmitPrecipioQuery(DeviceDateTableCreateQuery)
# print(paste('Table creation response:',TableCreateResponse))
# 
# for(iter in 1:nrow(SimpleDeviceInstallTable)){
#   # for(iter in 1:5){
#   InsertQuery<-paste("INSERT INTO [reporting.data].dbo.DeviceInstallDates VALUES ('",SimpleDeviceInstallTable$DeviceID[iter],"','",SimpleDeviceInstallTable$DeviceInstallDate[iter],"')",sep="")
#   print(paste('InsertQuery',iter,'of',nrow(SimpleDeviceInstallTable),':',InsertQuery))
#   QueryResponse<-SubmitPrecipioQuery(InsertQuery)
#   print(paste('QueryResponse:',QueryResponse))
# }

####  Find the most recent reading - used for time-since-last report
MostRecentTemperatureQuery<-'select top 1 ReadingDate from [precipio.snapshot.readings].dbo.Temperature T with(nolock) order by TemperatureID desc'
MostRecentReadingDate<-SubmitPrecipioQuery(MostRecentTemperatureQuery)

####   Generate a complete data set about Devices and Stores
DeviceAndStoreDF_DevInstall<-merge(DeviceAndStoreDF_Base,FirstMsgTable[,c('StrDeviceID','DeviceInstallDate')],by='StrDeviceID',all=TRUE)
DaysSinceLastReport<-round(difftime(strptime(MostRecentReadingDate,format='%Y-%m-%d %H:%M:%S'),DeviceAndStoreDF_DevInstall$LastActivity,units='days'),0)
DeviceAndStoreDF_DevInstall$DaysSinceLastReport<-ifelse(is.na(DaysSinceLastReport),NA,pmax(0,round(DaysSinceLastReport)))
write.csv(DeviceAndStoreDF_DevInstall,file=Filename_DeviceAndStoreDW_OldFields,row.names=FALSE)


