source('.Rprofile')
NetworkUtilsBaseDir<-file.path(GitHomeDir,'Utilities/Network Data')
source(file.path(NetworkUtilsBaseDir,'NetworkSignalQuality_AccessAndParsing.R'))
EagleEyeResultsDir<-file.path(OperationEagleEye_CubbyBaseDir,'Data Sets')


thisStoreSigStatsDF<-ExtractSigStrengthStatsForStoreGroupID(580)
write.csv(thisStoreSigStatsDF,file.path(EagleEyeResultsDir,'Store 1010/Store1010_SignalStats.csv'),row.names=FALSE)
