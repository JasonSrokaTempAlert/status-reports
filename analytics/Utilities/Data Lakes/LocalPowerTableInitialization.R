library(data.table)
source(file.path(GitHomeDir,'Utilities/Data Access','DevSnapshotAccess.R'))


SetupLocalPowerHistoryTable<-function(ClientLabel,AccountID,MinReadingDate=NULL,LocalPowerFile=NULL){
  DataQuery<-paste("select P.PowerID, P.DeviceID, P.Reading, P.ReadingDate, P.CreateDate from [precipio.snapshot.readings].dbo.[Power] P with(nolock) 
join [precipio.snapshot.accounts].dbo.Device D with(nolock) on P.DeviceID=D.DeviceID
  where D.AccountID=",AccountID,"and P.CommandID=80")
  DataQuery<-ifelse(is.null(MinReadingDate),DataQuery,paste(DataQuery,"and P.ReadingDate > '",MinReadingDate,"'"))

  if(is.null(LocalPowerFile)){
    LocalPowerFile<-file.path('D:/Local_Data_Store',paste(ClientLabel,'_PowerHistory.csv',sep=''))
  }
  PowerData<-SubmitPrecipioQuery(DataQuery)
  write.csv(PowerData,file=LocalPowerFile, row.names=FALSE)  
}

# SetupLocalPowerHistoryTable('Costco',5250)

SetupLocalHistoryTable_DailyPowerScoreAggs<-function(ClientLabel,AccountID,MinReadingDate=NULL,LocalPowerFile=NULL){
  if(is.null(LocalPowerFile)){
    LocalPowerFile<-file.path('D:/Local_Data_Store',paste(ClientLabel,'_DailyPowerAveHistory_InitialSQL.csv',sep=''))
  }


  # DataQuery<-paste("select P.DeviceID, cast(P.ReadingDate as date) ReadingDate, count(P.Reading) BattPower_DailyCount, round(avg(cast(Reading as decimal(3,1))),1) BattPower_DailyMean, 
  DataQuery<-paste("select P.DeviceID, cast(P.ReadingDate as date) ReadingDate, count(P.Reading) BattPower_DailyCount, avg(cast(Reading as decimal(3,1))) BattPower_DailyMean, 
                   min(Reading) BattPower_DailyMin, max(Reading) BattPower_DailyMax
from POWER_DB P with(nolock) 
join [precipio.snapshot.accounts].dbo.Device D with(nolock) on P.DeviceID=D.DeviceID
where D.AccountID=",AccountID,"and P.CommandID=80")
  
  DataQuery<-ifelse(is.null(MinReadingDate),DataQuery,paste(DataQuery,"and P.ReadingDate > '",MinReadingDate,"'"))
  DataQuery<-paste(DataQuery,'group by  P.DeviceID, cast(P.ReadingDate as date)')
  
  PowerData<-data.table(SubmitPowerQuery(DataQuery))
  setkey(PowerData,ReadingDate,DeviceID)
  PrefilterRows<-nrow(PowerData)
  MaxDate<-max(as.Date(PowerData$ReadingDate))
  PowerData<-PowerData[ReadingDate<MaxDate]
  
  DuplicatedIndices<-duplicated(PowerData)|duplicated(PowerData,fromLast = TRUE)
  DuplicatedData=PowerData[DuplicatedIndices,]
  DuplicatedData$BattPower_DailyMean=as.numeric(DuplicatedData$BattPower_DailyMean)
  DeDupedData<-DuplicatedData[,.(BattPower_DailyCount=sum(BattPower_DailyCount),
                                 BattPower_DailyMean=weighted.mean(BattPower_DailyMean,BattPower_DailyCount),
                                 BattPower_DailyMin=min(BattPower_DailyMin),
                                 BattPower_DailyMax=max(BattPower_DailyMax)
                                 ),by=.(DeviceID,ReadingDate)]
  
  FinalData<-rbind(PowerData[!DuplicatedIndices],DeDupedData)
  setkey(FinalData,ReadingDate,DeviceID)
  PostfilterRows<-nrow(PowerData)
  # print(paste('Prefilter rows:',PrefilterRows,'Postfilter rows:',PostfilterRows))
  
  print(paste('Writing FinalData to file:',LocalPowerFile))
  write.csv(FinalData,file=LocalPowerFile, row.names=FALSE)  
}

SetupLocalHistoryTable_DailyPowerScoreAggs('costco',5250)
SetupLocalHistoryTable_DailyPowerScoreAggs('walmart',4486)
SetupLocalHistoryTable_DailyPowerScoreAggs('cvs',4226)
