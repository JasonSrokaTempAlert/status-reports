
# coding: utf-8

####  This is a development cell to structure the input arguments before turning the rest into a function
# CustomerLabels=['walmart']
thisClientLabel='Walmart'
thisProcessLabel='DailyPowerAveHistory'
# thisClientLabel='Costco'

def ConvertTimeStampToSQLForm(FullTimestamp):
    NewTimestamp=FullTimestamp.replace('T',' ')
    NewTimestamp=NewTimestamp.replace('Z','')
    NewTimestamp=NewTimestamp.rstrip('0')
    return NewTimestamp

####  This cell sets things that should otherwise be moved into a configuration resource
##TODO  Create a config file or otherwise review/refine this

####  EC2 Instance Configuration
credentials_folder='D:/Trundle_Config_Files/config_files'
DataStoreDir='D:/Local_Data_Store'

Results_BaseDir='D:/StatusReports'

TempPowerFile=os.path.join(DataStoreDir,'Incremental_'+thisProcessLabel+'_'+thisClientLabel+'.csv')

####     Environment
import os
from trundle.auth import Auth
from trundle.client import Client
import json
from datetime import datetime
import csv
from pandas import pandas as pd
from datetime import timedelta

####  Read the Local Power Data File to get the latest Process date


# thisClientPowerDataFile="{}_PowerHistory.csv".format(thisClientLabel)
thisClientPowerDataFile=thisClientLabel+'_'+thisProcessLabel+'.csv'
DataFile=os.path.join(DataStoreDir,thisClientPowerDataFile)
##  Reading the Data
print 'Reading from file: {}'.format(DataFile)
PowerData=pd.read_csv(DataFile,dtype={'PowerID':str})
##  Extract the most recent CreateDate
thisLatestReadingDate=PowerData['ReadingDate'].max()
print 'Found most recent ReadingDate: {}'.format(thisLatestReadingDate)

PowerData.head()
PowerData["ReadingDate"].unique().max()

#    Incremental Power Access

from datetime import datetime
##  Set up client
if type(thisLatestReadingDate) is not datetime:
    thisLatestReadingDate=datetime.strptime(thisLatestReadingDate,'%Y-%m-%d')
IncrementalQueryStartingDate=thisLatestReadingDate + timedelta(days=1)
if (IncrementalQueryStartingDate+timedelta(days=1.1)>datetime.now()):
    print "Not ready to run for new day, table is through {}".format(thisLatestReadingDate)
    create.error()
    
##  Build the credentials file name
thisCredentialsFile=os.path.join(credentials_folder,thisClientLabel+'.json')
##  Two-step authorization and client construction
print 'Getting authorization'
thisAuth=Auth.from_json_file(thisCredentialsFile)
print 'Creating client'
thisDataClient = Client(thisAuth)
print 'Done with client setup'

##  Formulate the Trundle Call to get incremental info
print 'Making trundle wrapper call to get_all_device_readings with start date: {}'.format(IncrementalQueryStartingDate)
DeviceData=thisDataClient.get_all_device_readings(reading_start_dt=IncrementalQueryStartingDate,command_ids=['80'],output_file=TempPowerFile)
# IncPowerData=thisDataClient.get_all_device_readings(reading_start_dt=thisLatestCreateDate,command_ids=['80'])
print 'get_all_device_readings call returned, reading into Data Frame'

####  Read in the incremental data and prep for processing
IncPowerData=pd.read_csv(TempPowerFile)
IncPowerData.loc[:,'readingDate']=IncPowerData['readingDate'].apply(ConvertTimeStampToSQLForm)
IncPowerData['ReadingDateOnly']=IncPowerData['readingDate'].astype(str).str[0:10]
IncPowerData.head()

##  Remove everything that would be incomplete day
CutoffDate=datetime.now().strftime('%Y-%m-%d')
DataToProcess=IncPowerData.loc[IncPowerData['ReadingDateOnly']<CutoffDate]
print "Entries before date filtering: {}  After date filtering: {}".format(len(IncPowerData),len(DataToProcess))

DataForDailyAves=DataToProcess[['deviceID','ReadingDateOnly','readingValue']]
DailyAverages=DataForDailyAves.groupby(['deviceID','ReadingDateOnly'])['readingValue'].mean().reset_index()
# DailyAverages.columns(['DeviceID','ReadingDate','AverageDailyPowerScore'])
DailyAverages=DailyAverages.round({'readingValue':0})
DailyAverages.head()

with open(DataFile, 'a') as f:
    DailyAverages.to_csv(f, header=False,columns=['deviceID','ReadingDateOnly','readingValue'],index=False)

####  Check that the resulting csv can be read
thisData=pd.read_csv(DataFile)

thisData.head()

for thisDeviceID in thisData['DeviceID'].unique():
    print "Examining stats for device: {}".format(thisDeviceID)
    thisDeviceData=thisData[thisData['DeviceID']==thisDeviceID]
    print thisDeviceData
    thisMinValue=min(thisDeviceData['DailyPowerScoreMean'])
    LastDateAvailable=max(thisDeviceData['ReadingDate'])
    thisLatestDailyMean=thisDeviceData[thisDeviceData['ReadingDate']==LastDateAvailable]['DailyPowerScoreMean'].values
    print type(thisLatestDailyMean)
    print "thisLatestDailyMean: {}".format(thisLatestDailyMean)
    if (thisLatestDailyMean>=10):
        Velocity=0
        LastScoreLatestDate=''
    else:
        LastScore=thisLatestDailyMean+1
        LastScoreLatestDate=thisDeviceData[thisDeviceData['DailyPowerScoreMean']==LastScore]['ReadingDate'].max()
    print "LastScoreLatestDate: {}".format(LastScoreLatestDate)
    break

####  Manipulation of data to test proper incremental handling
PowerData.head()
PowerData["ReadingDate"].unique().max()
AllDates=PowerData["ReadingDate"].unique()
thisDict={}
for thisDate in AllDates:
#     print "thisDate: {}".format(thisDate)
    theseEntries=PowerData[PowerData["ReadingDate"]==thisDate]
#     print theseEntries
    thisDateNumEntries = len(theseEntries["DailyPowerScoreMean"])
    thisDict[thisDate]=thisDateNumEntries
#     break


###  This is code that was originally executed to assist in checking the outputs were valid - commenting out but retaining in case it could be useful again
##print thisDict
##keylist = thisDict.keys()
##keylist.sort()
##for key in keylist:
##    print "%s: %s" % (key, thisDict[key])
##
##print thisDict["2017-01-01"]
##
##print "thisData number of lines: {}".format(len(PowerData))
##newData=PowerData[PowerData["ReadingDate"]!="2017-01-02"]
##print "newData number of lines: {}".format(len(newData))
##newData=PowerData[PowerData["ReadingDate"]=="2017-01-02"]
##print "newData number of lines: {}".format(len(newData))
##
##len(PowerData[PowerData["ReadingDate"]!="2017-01-02"])
##
##print newData
##
##print PowerData
##
##744344-746065
##
##print len(newData)
##
##with open(DataFile, 'w') as f:
##    newData.to_csv(f, header=True,columns=['DeviceID','ReadingDate','DailyPowerScoreMean'],index=False)
##
##newData.head()



